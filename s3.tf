resource "aws_s3_bucket" "b" {
  bucket = "test-max-tf-bucket-999"
  acl    = "private"

  tags = {
    Name        = "Terraform Provisioned Bucket 999"
    Environment = "Dev"
    Application = "Test"
  }
}
